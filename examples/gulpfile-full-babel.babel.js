/* eslint-disable import/no-extraneous-dependencies */ // please remove line in your project, just for this repository
import gulp from "gulp";
import path from "path";
import merge from "merge-stream";
import del from "del";
import filter from "gulp-filter";
import concat from "gulp-concat"; // Concatenates JS files.
import uglifyJS from "gulp-uglify"; // Minifies JS files.
import babel from "gulp-babel"; // Compiles ESNext to browser compatible JS.
import plumber from "gulp-plumber"; // Prevent pipe breaking caused by errors from gulp plugins.
import rename from "gulp-rename"; // Renames files E.g. style.css -> style.min.css.
import sourcemaps from "gulp-sourcemaps"; // Maps code in a compressed file (E.g. style.css) back to it’s original position in a source file (E.g. structure.scss, which was later combined with other css files to generate style.css).
import sass from "gulp-sass"; // Gulp plugin for Sass compilation.
import uglifyCSS from "gulp-uglifycss"; // Minifies CSS files.
import autoprefixer from "gulp-autoprefixer"; // Autoprefixing magic.

import imagemin from "gulp-imagemin"; // Minify PNG, JPEG, GIF and SVG images with imagemin.

import sort from "gulp-sort"; // Recommended to prevent unnecessary changes in pot-file.
import wpPot from "gulp-wp-pot"; // For generating the .pot file.
import errorHandler from "../util/error";

import copyFiles from "../util/copy-files";

const config = {
  srcDir: "./src",
  dstDir: "./dst",
  browsers: ["last 2 versions", "> 2%"],
  babelPreset: "@babel/preset-env",
  babelBrowsers: null,
  jsBundleName: "bundle",
  otherFiles: [
    "src/other/**/*"
    // other examples:
    // {
    //   origPath: ["node_modules/optinout.js/dist/optinout.js"],
    //   path: "web/app/themes/efs/assets/libs/"
    // },
    // {
    //   origPath: ["node_modules/optinout.js/dist/optinout.js"],
    //   base: "node_modules/optinout.js",
    //   path: "web/app/themes/efs/assets/libs/"
    // }
  ]
};

config.cssSrc = `${config.srcDir}/css/**/*.scss`;
config.jsSrc = `${config.srcDir}/js/**/*.js`;

/**
 * Copies some other files (e.g. used for fonts, font-awesome icons,...) into the dist directory
 */
export const copyOtherFiles = () => {
  const parsedFiles = copyFiles(config.otherFiles);
  const tasks = [];

  parsedFiles.forEach(fileSet => {
    tasks.push(
      gulp
        .src(fileSet.src, { base: fileSet.base || null })
        .pipe(gulp.dest(fileSet.dest || config.dstDir))
    );
  });

  return merge(tasks);
};

/**
 * Concatenate, compiles and minifies scripts and adds sourcemaps.
 */
export const scripts = () =>
  gulp
    .src(config.jsSrc, { base: path.join(config.srcDir, "js") })
    .pipe(plumber({ errorHandler }))
    .pipe(sourcemaps.init())
    .pipe(
      babel({
        presets: [[config.babelPreset, { targets: config.babelBrowsers }]]
      })
    )
    .pipe(concat(`${config.jsBundleName}.js`))
    .pipe(sourcemaps.write("./"))
    .pipe(gulp.dest(path.join(config.dstDir, "js")))
    .pipe(filter("**/*.js")) // remove maps from stream
    .pipe(rename({ suffix: ".min" }))
    .pipe(uglifyJS())
    .pipe(sourcemaps.write("./"))
    .pipe(gulp.dest(path.join(config.dstDir, "js")));

/**
 * Concatenate, compiles and minifies stiles and adds sourcemaps.
 */
export const styles = () =>
  gulp
    .src(path.join(config.srcDir, "css", "main.scss"), {
      base: path.join(config.srcDir, "css")
    })
    .pipe(plumber({ errorHandler }))
    .pipe(sourcemaps.init())
    .pipe(sass({ includePaths: ["node_modules"] }))
    .pipe(autoprefixer(config.browsers))
    .pipe(sourcemaps.write("./"))
    .pipe(gulp.dest(path.join(config.dstDir, "css")))
    .pipe(filter("**/*.css")) // remove maps from stream
    .pipe(rename({ suffix: ".min" }))
    .pipe(uglifyCSS())
    .pipe(sourcemaps.write("./"))
    .pipe(gulp.dest(path.join(config.dstDir, "css")));

/**
 * Optimizes all images
 */
export const optimizeImages = () =>
  gulp
    .src(path.join(config.srcDir, "images", "**/*"), {
      base: path.join(config.srcDir, "images")
    })
    .pipe(plumber({ errorHandler }))
    .pipe(
      imagemin([
        imagemin.gifsicle({ interlaced: true }),
        imagemin.jpegtran({ progressive: true }),
        imagemin.optipng({ optimizationLevel: 3 }),
        imagemin.svgo({
          plugins: [{ removeViewBox: true }, { cleanupIDs: false }]
        })
      ])
    )
    .pipe(gulp.dest(path.join(config.dstDir, "images")));

/**
 * Translates WordPress php files to pot files.
 */
export const translateWordPress = () =>
  gulp
    .src(path.join(config.srcDir, "php", "**/*.php"), {
      base: path.join(config.srcDir, "php")
    })
    .pipe(plumber({ errorHandler }))
    .pipe(sort())
    .pipe(wpPot())
    .pipe(gulp.dest(path.join(config.dstDir, "languages", "translation.pot")));

/**
 * Cleans the dist directory.
 */
export const clean = () => del(config.dstDir);

/**
 * Watches CSS, JS and image directories for changes and calls the respective task.
 */
const watch = () => {
  gulp.watch(config.cssSrc, styles);
  gulp.watch(config.jsSrc, scripts);
  gulp.watch(path.join(config.srcDir, "images", "**/*"), optimizeImages);
};

/**
 * Build Task: Clean dist directory, copy other files (before everything else if src files depend on it) and build scripts, styles, images and translations.
 */
export const build = done =>
  gulp.series(
    clean,
    copyOtherFiles,
    gulp.parallel(scripts, styles, optimizeImages, translateWordPress)
  )(done);

/**
 * Develop Task: Call build task (see above) and watch for changes.
 */
export const develop = done => gulp.series(build, watch)(done);

/**
 * Export develop task as default.
 */
export default develop;
