/* eslint-disable import/no-extraneous-dependencies */ // please remove line in your project, just for this repository
import gulp from "gulp";
import path from "path";
import merge from "merge-stream";
import del from "del";
import filter from "gulp-filter";
import {
  build as buildScripts,
  minify as minifyScripts
} from "../tasks/scripts";
import { build as buildStyles, minify as minifyStyles } from "../tasks/styles";
import images from "../tasks/images";
import translate from "../tasks/translate";
import copyFiles from "../util/copy-files";

const config = {
  srcDir: "./src",
  dstDir: "./dst",
  browsers: ["last 2 versions", "> 2%"],
  otherFiles: [
    "src/other/**/*"
    // other examples:
    // {
    //   origPath: ["node_modules/optinout.js/dist/optinout.js"],
    //   path: "web/app/themes/efs/assets/libs/"
    // },
    // {
    //   origPath: ["node_modules/optinout.js/dist/optinout.js"],
    //   base: "node_modules/optinout.js",
    //   path: "web/app/themes/efs/assets/libs/"
    // }
  ]
};

config.cssSrc = `${config.srcDir}/css/**/*.scss`;
config.jsSrc = `${config.srcDir}/js/**/*.js`;

/**
 * Copies some other files (e.g. used for fonts, font-awesome icons,...) into the dist directory
 */
export const copyOtherFiles = () => {
  const parsedFiles = copyFiles(config.otherFiles);
  const tasks = [];

  parsedFiles.forEach(fileSet => {
    tasks.push(
      gulp
        .src(fileSet.src, { base: fileSet.base || null })
        .pipe(gulp.dest(fileSet.dest || config.dstDir))
    );
  });

  return merge(tasks);
};

/**
 * Concatenate, compiles and minifies scripts and adds sourcemaps.
 */
export const scripts = () =>
  gulp
    .src(config.jsSrc, { base: path.join(config.srcDir, "js") })
    .pipe(buildScripts({ bundleFilename: "bundle" })())
    .pipe(gulp.dest(path.join(config.dstDir, "js")))
    .pipe(filter("**/*.js")) // remove maps from stream
    .pipe(minifyScripts()())
    .pipe(gulp.dest(path.join(config.dstDir, "js")));

/**
 * Concatenate, compiles and minifies stiles and adds sourcemaps.
 */
export const styles = () =>
  gulp
    .src(path.join(config.srcDir, "css", "main.scss"), {
      base: path.join(config.srcDir, "css")
    })
    .pipe(buildStyles({ browsers: config.browsers })())
    .pipe(gulp.dest(path.join(config.dstDir, "css")))
    .pipe(filter("**/*.css")) // remove maps from stream
    .pipe(minifyStyles()())
    .pipe(gulp.dest(path.join(config.dstDir, "css")));

/**
 * Optimizes all images
 */
export const optimizeImages = () =>
  gulp
    .src(path.join(config.srcDir, "images", "**/*"), {
      base: path.join(config.srcDir, "images")
    })
    .pipe(images()())
    .pipe(gulp.dest(path.join(config.dstDir, "images")));

/**
 * Translates WordPress php files to pot files.
 */
export const translateWordPress = () =>
  gulp
    .src(path.join(config.srcDir, "php", "**/*.php"), {
      base: path.join(config.srcDir, "php")
    })
    .pipe(translate()())
    .pipe(gulp.dest(path.join(config.dstDir, "languages", "translation.pot")));

/**
 * Cleans the dist directory.
 */
export const clean = () => del(config.dstDir);

/**
 * Watches CSS, JS and image directories for changes and calls the respective task.
 */
const watch = () => {
  gulp.watch(config.cssSrc, styles);
  gulp.watch(config.jsSrc, scripts);
  gulp.watch(path.join(config.srcDir, "images", "**/*"), optimizeImages);
};

/**
 * Build Task: Clean dist directory, copy other files (before everything else if src files depend on it) and build scripts, styles, images and translations.
 */
export const build = done =>
  gulp.series(
    clean,
    copyOtherFiles,
    gulp.parallel(scripts, styles, optimizeImages, translateWordPress)
  )(done);

/**
 * Develop Task: Call build task (see above) and watch for changes.
 */
export const develop = done => gulp.series(build, watch)(done);

/**
 * Export develop task as default.
 */
export default develop;
