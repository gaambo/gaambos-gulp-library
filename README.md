# Gaambos Gulp

A library of handy functions to use in [gulp](https://gulpjs.org/) tasks. Built with [lazypipe](https://github.com/OverZealous/lazypipe) to easy embedd in your gulp tasks and unit-tested.
Originally built for WordPress projects but can be used with any web project.

## Installation

You can install the [npm package](https://www.npmjs.com/package/gaambos-gulp) via `npm install gaambos-gulp`. This includes only files needed to work in your project and the example folder.
You can download the [latest release here](https://github.com/gaambo/gaambos-gulp/releases/latest). This includes all files including tests.

### Requirements

The library is built in modern JavaScript (using ES6+) and therefore best used with Gulp 4 (and as `gulpfile.babel.js` to use ES6).

## Usage

The library exports ES Modules and is not transpiled/compiled/bundled. Therefore you can just include the files/paths you need.

### Main Functions

The following functions are defined as ES6 Modules in the `tasks` directory/namespace:

- `scripts`
  - `build`: Concatenate and compiles scripts (via [babel](https://babeljs.io/)) and adds sourcemaps.
  - `minify`: Minifies/uglifies scripts via [gulp-uglify](https://github.com/terinjokes/gulp-uglify), adds `.min` suffix and adds sourcemaps.
- `styles`
  - `build`: Compiles and autoprefixes (via [gulp-autoprefixer](https://github.com/sindresorhus/gulp-autoprefixer)) styles and adds sourcemaps.
  - `minify`: Minifies/uglifies styles via [gulp-uglifycss](https://github.com/ubirak/gulp-uglifycss), adds `.min` suffix and adds sourcemaps.
- `images`: Optimizes Images via [imagemin](https://github.com/imagemin/imagemin).
- `translate`: Generates pot files for WordPress .php files via [gulp-wp-pot](https://github.com/rasmusbe/gulp-wp-pot).

You can import any of those functions via ES import:

```js
import {
  build as buildScripts,
  minify as minifyScripts
} from "gaambos-gulp/tasks/scripts";
```

Alls "tasks" are built with [lazypipe](https://github.com/OverZealous/lazypipe) and return lazypipe pipelines. To actually run the pipeline the returned pipeline needs to be called.

### Helper Functions

The `util` directory/namespace contains util/helper functions to use in your gulpfile:

- `copy-files`: Parses an array of files to copy. The files are often defined in an `assets.json` or `build.json` and can have the following formats.
- `error`: A error handler method to be used with plumber. Sends a notification via [gulp-notify](https://github.com/mikaelbr/gulp-notify).
- `browsersync`: Functions to initialize and reload [browsersync](https://www.browsersync.io/). (**#TODO**)

### Full Gulpfile

I decided to add a full gulpfile without the usage of this library as well. It's not automatically built but just combines all the functions/tasks by hand. It's available in `examples/gulpfile-full-babel.babel.js` (the filename needs to be like that so babel transpiles it when calling).

### Example

See the `examples` directory for an example `gulpfile`. This gulpfile is written for Gulp 4 and exports all tasks as ES6 exports.
You can copy this gulpfile into your project and use it instantly.
The example gulpfile can be run via `npm run example`.
The full gulpfile can be run via `npm run full-example`.
Both npm tasks pass other arguments (eg task name) on to gulp.

## Development

Contribution to this package is welcome. Just clone the repository, install all dependencies via npm and develop. There's no transpiling/compiling used.
Unit tests can be run via `npm run tests`. The examples can be run via `npm run example`.
ESLint is used with airbnb and prettier configuration.

## Why

You may ask: Why the hell should you build a package/library for your build tasks and even write unit tests (which are more code than the actual tasks)?
Well, that's a very good question. The answer is easy though:

1. I use those tasks in a few different boilerplates (WordPress Vanilla, WordPress via Composer, simple sites via PHP Slim,...) and it makes it easier to reuse the logic and just define different directorie structures/configs. I could as well just publish my gulpfile and reuse it ([and there even is one in this repository](#full-gulpfile)), but see #2
2. I liked to develop a small open source project (additionally to my existing one [OptInOut](https://optinout.gitlab.io/)) and deepen my development skills in JavaScript and with unit testing.

## Licenses

The images in the example directory are taken from:

- `giphy.gif`: [giphy.com](https://giphy.com/gifs/running-muppets-7kn27lnYSAE9O)
- `picsum.photos.jpg`: [picsum.photos](http://picsum.photos/)
- `optinout.png` and `optinout.svg: From my OpenSource project [OptInOut](https://optinout.gitlab.io/)
