import copy from "../util/copy-files";

describe("copy files", () => {
  afterEach(() => {
    // reset all calledWith counters on mocks
    jest.clearAllMocks();
  });

  test("parse files with string src", async () => {
    const parsedFiles = copy(["src/images/**/*", "src/favicon/*"]);
    expect(parsedFiles[0]).toEqual({
      src: "src/images/**/*",
      base: "src",
      dest: null
    });
    expect(parsedFiles[1]).toEqual({
      src: "src/favicon/*",
      base: "src",
      dest: null
    });
  });

  test("copy with object src", async () => {
    const parsedFiles = copy([
      {
        origPath: ["node_modules/optinout.js/dist/optinout.js"],
        path: "web/app/themes/efs/assets/libs/"
      }
    ]);
    expect(parsedFiles[0]).toEqual({
      src: ["node_modules/optinout.js/dist/optinout.js"],
      base: null,
      dest: "web/app/themes/efs/assets/libs/"
    });
  });

  test("copy with object src with base", async () => {
    const parsedFiles = copy([
      {
        origPath: ["node_modules/optinout.js/dist/optinout.js"],
        base: "node_modules/optinout.js",
        path: "web/app/themes/efs/assets/libs/"
      }
    ]);
    expect(parsedFiles[0]).toEqual({
      src: ["node_modules/optinout.js/dist/optinout.js"],
      base: "node_modules/optinout.js",
      dest: "web/app/themes/efs/assets/libs/"
    });
  });
});
