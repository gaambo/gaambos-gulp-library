import sass from "gulp-sass";
import sourcemaps from "gulp-sourcemaps";
import rtl from "gulp-rtlcss";
import autoprefixer from "gulp-autoprefixer";
import rename from "gulp-rename";
import uglifycss from "gulp-uglifycss";
import plumber from "gulp-plumber";
import { build as buildStyles, minify as minifyStyles } from "../tasks/styles";
import { testGulpFromString, mockStream } from "./util";
import errorHandler from "../util/error";

// only for performance reasons
jest.mock("gulp-sass");
sass.mockImplementation(mockStream);

jest.mock("gulp-sourcemaps");
sourcemaps.init.mockImplementation(mockStream);
sourcemaps.write.mockImplementation(mockStream);

jest.mock("gulp-autoprefixer");
autoprefixer.mockImplementation(mockStream);

jest.mock("gulp-rename");
rename.mockImplementation(mockStream);

jest.mock("gulp-rtlcss");
rtl.mockImplementation(mockStream);

jest.mock("gulp-uglifycss");
uglifycss.mockImplementation(mockStream);

jest.mock("gulp-plumber");
plumber.mockImplementation(mockStream);

const scssInput =
  "$red: blue; body { background: $red; } div.bg { background: blue; }";

describe("styles", () => {
  afterEach(() => {
    // reset all calledWith counters on mocks
    jest.clearAllMocks();
  });

  test("build with errorhandler", async () => {
    await testGulpFromString(scssInput, "src/main.scss", buildStyles());
    expect(plumber).toBeCalledWith({ errorHandler });
  });

  test("build through sass", async () => {
    await testGulpFromString(scssInput, "src/main.scss", buildStyles());
    expect(sass).toBeCalled();
  });

  test("build with sourcemaps", async () => {
    await testGulpFromString(scssInput, "src/main.scss", buildStyles());
    expect(sourcemaps.init).toBeCalled();
    expect(sourcemaps.write).toBeCalled();
  });

  test("build with custom preprocessor", async () => {
    const preprocessor = jest.fn().mockImplementation(mockStream);

    await testGulpFromString(
      scssInput,
      "src/main.scss",
      buildStyles({ preprocessor })
    );
    expect(preprocessor).toBeCalled();
  });

  test("build rtl styles", async () => {
    await testGulpFromString(
      scssInput,
      "src/main.scss",
      buildStyles({ rtl: true })
    );
    expect(rtl).toBeCalled();
    expect(rename).toBeCalledWith({ suffix: "-rtl" });
  });

  test("build with autoprefixer", async () => {
    const browsers = ["last 1 version"];
    await testGulpFromString(
      scssInput,
      "src/main.scss",
      buildStyles({ browsers })
    );
    expect(autoprefixer).toBeCalledWith(browsers);
  });

  test("build with another name", async () => {
    const name = "test-bundle";
    await testGulpFromString(
      scssInput,
      "src/main.scss",
      buildStyles({ bundleFilename: name })
    );
    expect(rename).toBeCalledWith({ basename: name });
  });

  test("build minified", async () => {
    await testGulpFromString(scssInput, "src/main.scss", minifyStyles());
    expect(uglifycss).toBeCalled();
    expect(rename).toBeCalledWith({ suffix: ".min" });
  });

  test("build minified with custom options", async () => {
    const uglifyOptions = {
      maxLineLen: 10
    };
    await testGulpFromString(
      scssInput,
      "src/main.scss",
      minifyStyles({ uglifyOptions })
    );
    expect(uglifycss).toBeCalledWith(uglifyOptions);
  });

  test("build minified with sourcemaps", async () => {
    await testGulpFromString(scssInput, "src/main.scss", minifyStyles());
    expect(sourcemaps.init).toBeCalled();
    expect(sourcemaps.write).toBeCalled();
  });
});
