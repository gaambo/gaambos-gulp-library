import vinyl from "vinyl-string";
import map from "map-stream";
import through from "through2";

/*
 * Get transformed contents of a string
 *
 * @param {string} input - String contents of the "file"
 * @param {string} path  - The "path" of the "file"
 * @param {function} func - The lazypipe that will be used to transform the input
 *
 * @returns {string} Vinyl file representing the original `input` and `path`, transformed by the `func`
 */
export const testGulpFromString = (input, path, func) => {
  return new Promise((res, rej) => {
    let contents = false; // So we can grab the content later

    const vFile = vinyl(input, { path }); // Equivalent to path: path. ES6 Object Literal Shorthand Syntax

    vFile
      .pipe(func()) // Call the function we're going to pass in
      .pipe(
        map((file, cb) => {
          contents = file;
          cb(null, file);
        })
      )
      .on("error", e => {
        rej(e);
      })
      .on("end", () => {
        res(contents);
      });
  });
};

export const mockStream = () => {
  return through.obj((file, encoding, callback) => {
    callback(null, file);
  });
};
