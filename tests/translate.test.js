import plumber from "gulp-plumber";
import sort from "gulp-sort";
import wpPot from "gulp-wp-pot";
import errorHandler from "../util/error";
import { testGulpFromString, mockStream } from "./util";

import translate from "../tasks/translate";

jest.mock("gulp-plumber");
plumber.mockImplementation(mockStream);

jest.mock("gulp-sort");
sort.mockImplementation(mockStream);

jest.mock("gulp-wp-pot");
wpPot.mockImplementation(mockStream);

describe("translate", () => {
  afterEach(() => {
    // reset all calledWith counters on mocks
    jest.clearAllMocks();
  });
  test("translate with errorhandler", async () => {
    await testGulpFromString("", "src/test.php", translate());
    expect(plumber).toBeCalledWith({ errorHandler });
  });

  test("translate with default options", async () => {
    await testGulpFromString("", "src/test.php", translate());
    expect(sort).toBeCalled();
    expect(wpPot).toBeCalled();
  });

  test("translate with custom options", async () => {
    const options = {
      textDomain: "gaambos-gulp",
      packageName: "gaambos-gulp",
      bugReport: "https://example.com",
      lastTranslator: "Fabian Todt <fabian@fabiantodt.at>",
      team: "FT <fabian@fabiantodt.at>"
    };

    const wpPotOptions = {
      domain: options.textDomain,
      package: options.packageName,
      bugReport: options.bugReport,
      lastTranslator: options.lastTranslator,
      team: options.team
    };

    await testGulpFromString("", "src/test.php", translate(options));
    expect(sort).toBeCalled();
    expect(wpPot).toBeCalledWith(wpPotOptions);
  });
});
