import imagemin from "gulp-imagemin"; // Minify PNG, JPEG, GIF and SVG images with imagemin.
import plumber from "gulp-plumber";
import errorHandler from "../util/error";
import { testGulpFromString, mockStream } from "./util";

import buildImages from "../tasks/images";

jest.mock("gulp-imagemin");
imagemin.mockImplementation(mockStream);

jest.mock("gulp-plumber");
plumber.mockImplementation(mockStream);

describe("images", () => {
  afterEach(() => {
    // reset all calledWith counters on mocks
    jest.clearAllMocks();
  });

  test("optimize with errorhandler", async () => {
    await testGulpFromString("", "src/test.gif", buildImages());
    expect(plumber).toBeCalledWith({ errorHandler });
  });

  test("call with custom plugin", async () => {
    const customPlugin = jest.fn();
    await testGulpFromString(
      "",
      "src/test.jpeg",
      buildImages({
        gif: false,
        jpeg: false,
        png: false,
        svg: false,
        plugins: [customPlugin]
      })
    );
    expect(imagemin).toBeCalledWith([customPlugin]);
    // expect(customPlugin).toBeCalled(); // does not work: would need a real image to test
  });

  describe("gif", () => {
    test("optimize gif with imagemin", async () => {
      await testGulpFromString("", "src/test.gif", buildImages());
      expect(imagemin.gifsicle).toBeCalled();
      expect(imagemin).toBeCalled();
    });

    test("optimize gif with imagemin with custom options", async () => {
      const options = { interlaced: false };
      await testGulpFromString(
        "",
        "src/test.gif",
        buildImages({ gif: options })
      );
      expect(imagemin.gifsicle).toBeCalledWith(options);
      expect(imagemin).toBeCalled();
    });

    test("do not optimize gif", async () => {
      await testGulpFromString("", "src/test.gif", buildImages({ gif: false }));
      expect(imagemin.gifsicle).not.toBeCalled();
      expect(imagemin).toBeCalled();
    });
  });

  describe("jpeg", () => {
    test("optimize jpeg with imagemin", async () => {
      await testGulpFromString("", "src/test.jpeg", buildImages());
      expect(imagemin.jpegtran).toBeCalled();
      expect(imagemin).toBeCalled();
    });

    test("optimize jpeg with imagemin with custom options", async () => {
      const options = { progressiv: false };
      await testGulpFromString(
        "",
        "src/test.jpeg",
        buildImages({ jpeg: options })
      );
      expect(imagemin.jpegtran).toBeCalledWith(options);
      expect(imagemin).toBeCalled();
    });

    test("do not optimize jpeg", async () => {
      await testGulpFromString(
        "",
        "src/test.jpeg",
        buildImages({ jpeg: false })
      );
      expect(imagemin.jpegtran).not.toBeCalled();
      expect(imagemin).toBeCalled();
    });
  });

  describe("png", () => {
    test("optimize png with imagemin", async () => {
      await testGulpFromString("", "src/test.png", buildImages());
      expect(imagemin.optipng).toBeCalled();
      expect(imagemin).toBeCalled();
    });

    test("optimize png with imagemin with custom options", async () => {
      const options = { optimizationLevel: 5 };
      await testGulpFromString(
        "",
        "src/test.png",
        buildImages({ png: options })
      );
      expect(imagemin.optipng).toBeCalledWith(options);
      expect(imagemin).toBeCalled();
    });

    test("do not optimize png", async () => {
      await testGulpFromString("", "src/test.png", buildImages({ png: false }));
      expect(imagemin.optipng).not.toBeCalled();
      expect(imagemin).toBeCalled();
    });
  });

  describe("svg", () => {
    test("optimize svg with imagemin", async () => {
      await testGulpFromString("", "src/test.svg", buildImages());
      expect(imagemin.svgo).toBeCalled();
      expect(imagemin).toBeCalled();
    });

    test("optimize svg with imagemin with custom options", async () => {
      const options = { plugins: [{ removeViewBox: false }] };
      await testGulpFromString(
        "",
        "src/test.svg",
        buildImages({ svg: options })
      );
      expect(imagemin.svgo).toBeCalledWith(options);
      expect(imagemin).toBeCalled();
    });

    test("do not optimize svg", async () => {
      await testGulpFromString("", "src/test.svg", buildImages({ svg: false }));
      expect(imagemin.svgo).not.toBeCalled();
      expect(imagemin).toBeCalled();
    });
  });
});
