import concat from "gulp-concat"; // Concatenates JS files.
import uglify from "gulp-uglify"; // Minifies JS files.
import babel from "gulp-babel"; // Compiles ESNext to browser compatible JS.
import sourcemaps from "gulp-sourcemaps";
import plumber from "gulp-plumber"; // Prevent pipe breaking caused by errors from gulp plugins.
import rename from "gulp-rename"; // Renames files E.g. style.css -> style.min.css.
import errorHandler from "../util/error";

import { testGulpFromString, mockStream } from "./util";
import {
  build as buildScripts,
  minify as minifyScripts
} from "../tasks/scripts";

jest.mock("gulp-concat");
concat.mockImplementation(mockStream);

jest.mock("gulp-uglify");
uglify.mockImplementation(mockStream);

jest.mock("gulp-babel");
babel.mockImplementation(mockStream);

jest.mock("gulp-plumber");
plumber.mockImplementation(mockStream);

jest.mock("gulp-rename");
rename.mockImplementation(mockStream);

jest.mock("gulp-sourcemaps");
sourcemaps.init.mockImplementation(mockStream);
sourcemaps.write.mockImplementation(mockStream);

const jsInput =
  "(function($){ await asyncTestFunction(); const $el = $('#el'); }(jQuery));";

describe("scripts", () => {
  afterEach(() => {
    // reset all calledWith counters on mocks
    jest.clearAllMocks();
  });
  test("build with errorhandler", async () => {
    await testGulpFromString(
      jsInput,
      "src/main.js",
      buildScripts({ bundleFilename: "bundle" })
    );
    expect(plumber).toBeCalledWith({ errorHandler });
  });

  test("build with sourcemaps", async () => {
    await testGulpFromString(
      jsInput,
      "src/main.js",
      buildScripts({ bundleFilename: "bundle" })
    );
    expect(sourcemaps.init).toBeCalled();
    expect(sourcemaps.write).toBeCalled();
  });

  test("build concatenated scripts", async () => {
    await testGulpFromString(
      jsInput,
      "src/main.js",
      buildScripts({ bundleFilename: "bundle" })
    );
    expect(concat).toBeCalled();
  });

  test("build scripts with babel", async () => {
    await testGulpFromString(
      jsInput,
      "src/main.js",
      buildScripts({ bundleFilename: "bundle" })
    );
    expect(babel).toBeCalled();
  });

  test("build scripts with custom babel preset", async () => {
    await testGulpFromString(
      jsInput,
      "src/main.js",
      buildScripts({
        bundleFilename: "bundle",
        babelPreset: "@babel/preset-es2015"
      })
    );
    expect(babel).toBeCalledWith({
      presets: [["@babel/preset-es2015", { targets: null }]]
    });
  });

  test("build scripts with custom babel browsers", async () => {
    const browsers = {
      edge: "17",
      firefox: "60",
      chrome: "67",
      safari: "11.1"
    };
    await testGulpFromString(
      jsInput,
      "src/main.js",
      buildScripts({ bundleFilename: "bundle", babelBrowsers: browsers })
    );
    expect(babel).toBeCalledWith({
      presets: [["@babel/preset-env", { targets: browsers }]]
    });
  });

  test("build scripts with custom babel options", async () => {
    const babelOptions = {
      presets: [
        [
          "@babel/preset-es2015",
          {
            targets: {
              edge: "17",
              firefox: "60",
              chrome: "67",
              safari: "11.1"
            }
          }
        ]
      ]
    };
    await testGulpFromString(
      jsInput,
      "src/main.js",
      buildScripts({
        bundleFilename: "bundle",
        customBabelOptions: babelOptions
      })
    );
    expect(babel).toBeCalledWith(babelOptions);
  });

  test("build minified scripts", async () => {
    await testGulpFromString(jsInput, "src/main.js", minifyScripts());
    expect(uglify).toBeCalled();
    expect(rename).toBeCalledWith({ suffix: ".min" });
  });

  test("build minified scripts with custom options", async () => {
    const uglifyOptions = {
      warnings: true
    };
    await testGulpFromString(
      jsInput,
      "src/main.js",
      minifyScripts({ uglifyOptions })
    );
    expect(uglify).toBeCalledWith(uglifyOptions);
  });

  test("build minified scripts with sourcemaps", async () => {
    await testGulpFromString(jsInput, "src/main.js", minifyScripts());
    expect(sourcemaps.init).toBeCalled();
    expect(sourcemaps.write).toBeCalled();
  });
});
