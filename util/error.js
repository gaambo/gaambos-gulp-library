import notify from "gulp-notify"; // Sends message notification to you.

/**
 * A error handler method to be used with plumber. Sends a notification via {@link gulp-notify}.
 */
export default r => {
  notify.onError("❌ ERROR: <%= error.message %>\n")(r);
};
