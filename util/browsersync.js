import browserSync from "browser-sync"; // Reloads browser and injects CSS. Time-saving synchronized browser testing.

/**
 * @todo needs to be tested and added to example gulpfile.
 */
export const browsersync = ({ projectURL, browserSyncConfig = {} }) => {
  browserSync.create();
  return done => {
    browserSync.init({
      proxy: projectURL,
      watchEvents: ["change", "add", "unlink", "addDir", "unlinkDir"],
      ...browserSyncConfig
    });
    done();
  };
};

/**
 * @todo needs to be tested and added to example gulpfile.
 * Helper function to allow browser reload with Gulp 4.
 */
export const reload = done => {
  browserSync.reload();
  done();
};
