import path from "path";

/**
 * Parses an array of files to copy.
 * The files are often defined in an `assets.json` or `build.json` and can have the following formats.
 * The files configuration can be passed as string or object.
 * If it's a string this string/glob will be used as src and the function will try to build a base if it has wildcards. The destination will be null (should be set by gulp.dest)
 * If it's an object the origPath and path will be used for src and dest, base is optional and will not be built.
 * The returned array can be used to build multiple gulp streams with {@link merge-stream}. See the example gulpfile.
 * @see examples directory for example configurations.
 *
 * @param {Object[]|string[]} files Different file configs to parse. If string can be a glob to pass to gulp.
 * @param {string} files[].origPath The source path of the file to copy.
 * @param {string} [files[].base] The base path to use for gulp.src
 * @param {string} files[].path The destination path to copy the file too.
 * @returns {Object[]} Array of file objects to be used in a gulp task. Each Object has a src, base and dest property.
 */
export default files => {
  const parsedFiles = [];

  files.forEach(el => {
    if (typeof el === "object") {
      parsedFiles.push({
        src: el.origPath,
        base: el.base || null,
        dest: el.path
      });
    } else {
      let srcPath = el;
      const wildcardPosition = el.indexOf("*");
      if (wildcardPosition) {
        srcPath = el.substring(0, wildcardPosition);
      }

      const baseDir = path.dirname(srcPath);
      parsedFiles.push({
        src: el,
        base: baseDir || null,
        dest: null
      });
    }
  });

  return parsedFiles;
};
