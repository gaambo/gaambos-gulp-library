import lazypipe from "lazypipe"; // Build reusable partial pipelines.
import plumber from "gulp-plumber"; // Prevent pipe breaking caused by errors from gulp plugins.
import sort from "gulp-sort"; // Recommended to prevent unnecessary changes in pot-file.
import wpPot from "gulp-wp-pot"; // For generating the .pot file.

import errorHandler from "../util/error";

/**
 * Generates pot files for WordPress .php files via {@link gulp-wp-pot}.
 * All options are optional.
 *
 * @param {Object} [options] All options
 * @param {string} [options.textDomain] Textdomain to retrieve the translated text. All textdomains if undefined/null.
 * @param {string} [options.packageName] Package name. Defaults to textdomain in wp-pot.
 * @param {string} [options.bugReport] URL for reporting translation bugs.
 * @param {string} [options.lastTranslator] Name and email address of the last translator.
 * @param {string} [options.team] Name and email address of the translation team.
 * @see {@link https://github.com/rasmusbe/wp-pot#options|wp-pot documentation}
 * @returns {Stream} A {@link lazypipe} pipeline. To actually run the pipeline the returned pipeline needs to be called.
 */
export default ({
  textDomain = null,
  packageName = null,
  bugReport = null,
  lastTranslator = null,
  team = null
} = {}) => {
  return lazypipe()
    .pipe(
      plumber,
      { errorHandler }
    )
    .pipe(sort)
    .pipe(
      wpPot,
      {
        domain: textDomain,
        package: packageName,
        bugReport,
        lastTranslator,
        team
      }
    );
};
