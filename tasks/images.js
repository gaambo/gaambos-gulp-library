import lazypipe from "lazypipe"; // Build reusable partial pipelines.
import plumber from "gulp-plumber"; // Prevent pipe breaking caused by errors from gulp plugins.
import imagemin from "gulp-imagemin"; // Minify PNG, JPEG, GIF and SVG images with imagemin.

import errorHandler from "../util/error";

/**
 * Optimizes Images via {@link imagemin}.
 * By default it uses the four plugins bundled with imagemin: gifsicle, jpegtran, optipng, svgo.
 * You can disable those individually, provide custom options for them or add custom imagemin plugins.
 *
 * @param {Object} [options] All options
 * @param {Object|boolean} [options.gif=true] Options for the gifsicle plugin, false if should be disabled. See source for default.
 * @param {Object|boolean} [options.jpeg=true] Options for the jpegtran plugin, false if should be disabled. See source for default.
 * @param {Object|boolean} [options.png=true] Options for the optipng plugin, false if should be disabled. See source for default.
 * @param {Object|boolean} [options.svg=true] Options for the svgo plugin, false if should be disabled. See source for default.
 * @param {Object[]} [options.plugins] Other imagemin plugins to use.
 * @returns {Stream} A {@link lazypipe} pipeline. To actually run the pipeline the returned pipeline needs to be called.
 */
export default ({
  gif = { interlaced: true },
  jpeg = { progressive: true },
  png = { optimizationLevel: 3 },
  svg = {
    plugins: [{ removeViewBox: true }, { cleanupIDs: false }]
  },
  plugins = [] // other custom plugins
} = {}) => {
  const imageminPlugins = [...plugins];

  if (gif) {
    imageminPlugins.push(imagemin.gifsicle(gif));
  }

  if (jpeg) {
    imageminPlugins.push(imagemin.jpegtran(jpeg));
  }

  if (png) {
    imageminPlugins.push(imagemin.optipng(png));
  }

  if (svg) {
    imageminPlugins.push(imagemin.svgo(svg));
  }

  return lazypipe()
    .pipe(
      plumber,
      { errorHandler }
    )
    .pipe(
      imagemin,
      imageminPlugins
    );
};
