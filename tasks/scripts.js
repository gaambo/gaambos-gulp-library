import lazypipe from "lazypipe"; // Build reusable partial pipelines.
import concat from "gulp-concat"; // Concatenates JS files.
import uglify from "gulp-uglify"; // Minifies JS files.
import babel from "gulp-babel"; // Compiles ESNext to browser compatible JS.
import plumber from "gulp-plumber"; // Prevent pipe breaking caused by errors from gulp plugins.
import rename from "gulp-rename"; // Renames files E.g. style.css -> style.min.css.
import sourcemaps from "gulp-sourcemaps"; // Maps code in a compressed file (E.g. style.css) back to it’s original position in a source file (E.g. structure.scss, which was later combined with other css files to generate style.css).

import errorHandler from "../util/error";

/**
 * Concatenate and compiles scripts (via {@link babel}) and adds sourcemaps.
 * By default it uses the `@babel/preset-env` preset but you can pass presets, browsers or other options to babel.
 *
 * @param {Object} [options] All options
 * @param {string} options.bundleFilename The filename of the concatinated bundle file.
 * @param {string} [options.babelPreset=@babel/preset-env] The babel preset to use.
 * @param {Object} [options.babelBrowsers=null] Object of browsers to be passed to babel targets. See {@link https://babeljs.io/docs/en/usage|Babel documentation}.
 * @param {Object} [options.customBabelOptions=null] Custom babelOptions to be passed to babel. Overwrites all other configurations and the other arguments above.
 * @returns {Stream} A {@link lazypipe} pipeline. To actually run the pipeline the returned pipeline needs to be called.
 */
export const build = ({
  bundleFilename,
  babelPreset = "@babel/preset-env",
  babelBrowsers = null,
  customBabelOptions = null
}) => {
  let babelOptions = {
    presets: [[babelPreset, { targets: babelBrowsers }]]
  };
  if (customBabelOptions) {
    babelOptions = customBabelOptions;
  }
  return lazypipe()
    .pipe(
      plumber,
      { errorHandler }
    )
    .pipe(sourcemaps.init)
    .pipe(
      babel,
      babelOptions
    )
    .pipe(
      concat,
      `${bundleFilename}.js`
    )
    .pipe(
      sourcemaps.write,
      "./"
    );
};

/**
 * Minifies/uglifies scripts via {@link gulp-uglify}, adds `.min` suffix and adds sourcemaps.
 * You can pass custom uglifyOptions to it, by default none are passed and the default uglify options are used.
 *
 * @param {Object} [options] All options
 * @param {Object} [options.uglifyOptions=null] Options to pass to uglify.
 * @returns {Stream} A {@link lazypipe} pipeline. To actually run the pipeline the returned pipeline needs to be called.
 */
export const minify = ({ uglifyOptions = null } = {}) => {
  return lazypipe()
    .pipe(
      sourcemaps.init,
      { loadMaps: true }
    )
    .pipe(
      rename,
      {
        suffix: ".min"
      }
    )
    .pipe(
      uglify,
      uglifyOptions
    )
    .pipe(
      sourcemaps.write,
      "./"
    );
};
