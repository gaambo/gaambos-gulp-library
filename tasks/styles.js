import lazypipe from "lazypipe"; // Build reusable partial pipelines.
import sass from "gulp-sass"; // Gulp plugin for Sass compilation.
import uglifycss from "gulp-uglifycss"; // Minifies CSS files.
import autoprefixer from "gulp-autoprefixer"; // Autoprefixing magic.
import rtlcss from "gulp-rtlcss"; // Generates RTL stylesheet.
import plumber from "gulp-plumber"; // Prevent pipe breaking caused by errors from gulp plugins.
import rename from "gulp-rename"; // Renames files E.g. style.css -> style.min.css.
import sourcemaps from "gulp-sourcemaps"; // Maps code in a compressed file (E.g. style.css) back to it’s original position in a source file (E.g. structure.scss, which was later combined with other css files to generate style.css).

import errorHandler from "../util/error";

/**
 * Compiles and autoprefixes (via {@link gulp-autoprefixer}) styles and adds sourcemaps.
 * By default sass (via {@link gulp-sass}) is used as a preprocessor, but you can pass in another one. YOu can also pass in custom preprocessor options.
 * You can build rtl styles with it too.
 *
 * @param {Object} [options] All options
 * @param {string[]} [options.browsers] Browserlist to be passed to autoprefixer. See {@link https://github.com/postcss/autoprefixer#browsers|Autoprefixer documentation}.
 * @param {function} [options.preprocessor=sass] CSS-Preprocessor function to use. Defaults to sass via {@link gulp-sass}.
 * @param {Object} [options.preprocessorOptions] Options to pass to preprocessor function. See source for defaults for sass.
 * @param {string} [options.bundleFilename=false] Optional filename to rename output bundle to.
 * @param {boolean} [options.rtl=false] Build rtl-stylesheets via {@link gulp-rtlcss}.
 * @returns {Stream} A {@link lazypipe} pipeline. To actually run the pipeline the returned pipeline needs to be called.
 */
export const build = ({
  browsers = null,
  preprocessor = sass,
  preprocessorOptions = { includePaths: ["node_modules"] },
  bundleFilename = false,
  rtl = false
} = {}) => {
  let task = lazypipe()
    .pipe(
      plumber,
      { errorHandler }
    )
    .pipe(sourcemaps.init)
    .pipe(
      preprocessor,
      {
        ...(preprocessorOptions || {})
      }
    );

  if (rtl) {
    task = task.pipe(rtlcss).pipe(
      rename,
      { suffix: "-rtl" }
    );
  }

  if (bundleFilename) {
    task = task.pipe(
      rename,
      { basename: bundleFilename }
    );
  }

  task = task
    .pipe(
      autoprefixer,
      browsers
    )
    .pipe(
      sourcemaps.write,
      "./"
    );

  return task;
};

/**
 * Minifies/uglifies styles via {@link gulp-uglifycss}, adds `.min` suffix and adds sourcemaps.
 * You can pass custom uglifyOptions to it, by default none are passed and the default uglify options are used.
 *
 * @param {Object} [options] All options
 * @param {Object} [options.uglifyOptions=null] Options to pass to uglify.
 * @returns {Stream} A {@link lazypipe} pipeline. To actually run the pipeline the returned pipeline needs to be called.
 */
export const minify = ({ uglifyOptions = {} } = {}) => {
  return lazypipe()
    .pipe(sourcemaps.init)
    .pipe(
      rename,
      { suffix: ".min" }
    )
    .pipe(
      uglifycss,
      uglifyOptions
    )
    .pipe(
      sourcemaps.write,
      "./"
    );
};
